[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Visualitza el volum del so

#### Gradient de colors

- En aquesta unitat farem una barra de colors, per representar el volum del so. La tira de NeoPíxels s'anirà il·luminant del verd al vermell, tot passant pel groc.

- Una manera de fer-ho seria comprovar el número de LED i assignar-li un color específic, però és un mètode poc eficient i gens escalable (val per 3 LEDs però no per 20).

- El model RGB defineix el color amb quantitats pel vermell, pel verd i pel blau, no permet passar fàcilment d'un color a un altre que no sigui un d'aquests colors fonamentals. Per canviar gradualment d'un color a altres existeix un model més adequat, el model de color HSV.

#### Model de color HSV

Per tractar aquest model de color és necessari importar la llibreria **Gràfics i pantalles→Color**:

![Llibreria Color](cm07-01-llibreria-color.png)

- El bloc **`color amb to _ (0-360) saturació _ % i brillantor _ %`**  permet obtenir un color especificant el **to o matís** (hue), **saturació** (saturation) i **brillantor o valor** (value).

![Bloc HSV](cm07-02-bloc-hsv.png)

- Si fixem els valors de saturació i brillantor al màxim (100) només cal que variem el to (H) de 0 a 360 per obtenir qualsevol color.
- Aquest bloc retorna un color en format RGB codificat, perquè els blocs de la llibreria NeoPíxel i d'altres, com TFT o Tortuga, puguin mostrar els colors resultants.

| ![](cm07-03-hsv-model1.png "Model HSV 1") | ![](cm07-04-hsv-model2.png "Model HSV 2") |
| ----------------------------------------- | ----------------------------------------- |

- Per aquest model el color vermell seria el 0, el verd el 60 i el blau el 240.

##### Repte 1: troba el rang de valors per passar de vermell a verd

![Challenge 1](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

**Vermell**

![Solució repte 1 vermell](cm07-s1a.png)

**Groc**

![Solució repte 1 groc](cm07-s1b.png)

**Verd**

![Solució repte 1 verd](cm07-s1c.png)

</details>

#### Mostrar el gradient de color

- Ja tenim que per generar l'efecte desitjat haurem de variar el to entre 0 i 120.

- Només queda fer servir aquest bloc en el programa de la unitat anterior, amb el potenciòmetre connectat, per tal d’obtenir el gradient de colors.

- Com que rang que volem és des de 0 fins a 120 i tenim 10 LEDs, haurem de fer servir un factor de 12.

![Gradient de colors](cm07-18-gradient.png)

- Aquest bloc haurà d'anar dintre d'un bloc de repetició, per exemple un bloc **`per sempre`** perquè funcioni d'una manera contínua. És recomanable posar una petita espera perquè la placa no faci més operacions de les necessàries.
- Però, l’ordre dels colors és el correcte?

##### Repte 2: inverteix l’ordre del gradient per a que comenci en verd i acabi en vermell

![Challenge 2](cm-challenge.png)

<details><summary>Solució al repte 2</summary>

![Solució repte 2](cm07-s2.png)

</details>

#### Micròfon

![Plaques](boards.svg) Algunes plaques porten micròfon incorporat: micro:bit V2, micro:STEAMakers, M5Stack-Core2, Boardie i d'altres.

- Ara canviarem el potenciòmetre per un micròfon, de manera que el volum del so que rebrà el veurem reflectit en el nombre de NeoPíxels il·luminats.

- Per fer servir el micròfon haurem d’obrir la llibreria **Sensors→Micròfon**:

![Llibreria micròfon](cm07-19-llibreria-microfon.png)

![Connexió micrófon](cm07-20-microfon-connexio.jpg)

#### Últims passos

- Finalment, només cal fer servir el bloc **`volum`** per captar el nivell del so que rep el micròfon.

- Podem “controlar” el nivell de sensibilitat d’aquest reduint el factor de divisió de 1023 a un valor més petit, per exemple a la meitat.

![Volum del so](cm07-21-volum-so.png)

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-06-ca) | [Activitat següent ➡️](../citilab-course-08-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
