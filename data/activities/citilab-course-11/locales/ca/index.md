[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Persiana controlada amb llum

![Plaques](boards.svg) Qualsevol placa amb connexions digitals pels servos i sensor de llum (micro:bit, micro:STEAM Makers i d'altres). Per a la simulació de la persiana es farà servir la pantalla TFT (Citilab ED1, M5Stack-Core i d'altres).

#### Blocs propis

- Una vegada fet el control amb els botons es pot canviar per qualsevol altre tipus de sensor, com pot ser un sensor de llum, un comandament, un control per Internet o per aplicació mòbil.

- Afegir un control extra i que sigui compatible amb el control per botons requereix complicar l'estructura del programa. Per això recomanen definir blocs propis que ajudin a simplificar el codi abans de començar aquest procés.

![Blocs propis](cm11-01-blocs-propis1.png)

#### Com crear blocs propis

- Per crear blocs propis cal  fer servir els botons **Crea un bloc comanda** o **Crea un bloc reportador** de la categoria **Els meus blocs**. Si el bloc ha de retornar algun valor haurem d'escollir el botó de bloc reportador i és necessari d'incloure un o més blocs **`retorna _`** de la categoria **Control**.

![Crear bloc propi](cm11-04-blocs-propis-crear.png)

- MicroBlocks ens demana el nom del bloc i en fabricarà un de nou, amb forma de barret, al que podem enganxar els blocs que defineixen l'acció del nou bloc.

![Nou bloc](cm11-05-blocs-propis-baixa.png)

- Picant amb el ratolí sobre la definició del bloc podem afegir paràmetres al nostre bloc.

![Paràmetres de bloc](cm11-06-blocs-propis-parametres.png)

- Una vegada definits, els blocs es poden llençar (no s'esborren) i així ens quedarà el nou programa més fàcil de comprendre, de modificar i d'ampliar.

![Persiana amb botons](cm11-02-base.png)

- Per modificar la definició del bloc, feu clic amb el botó dret del ratolí sobre el bloc i trieu **mostra la definició del bloc**.

![Mostrant definció del bloc](cm11-07-blocs-propis-modifica.png)

#### La lectura del sensor s'ha de fer a intervals

- En aquest cas farem servir el bloc **`nivell de llum`** que trobarem a la categoria **Sensors bàsics**.

- La lectura del sensor s'ha de fer amb intervals de temps grans, per donar temps a pujar i abaixar completament la persiana i evitar també canvis mínims en la il·luminació.

- Pot ser interessant definir un bloc per espera minuts en comptes de mil·lisegons.

![Bloc minuts](cm11-03-bloc-minuts.png)

##### Repte 1: completa el projecte de la persiana amb control per teclat i sensor de llum. La persiana s'atura si es prem qualsevol dels dos botons.

![Challenge 1](cm-challenge.png)

<details>
  <summary>Solució al repte 1</summary>

![Solució repte 1](cm11-s1.png)

</details>

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-10-ca) | [Activitat següent ➡️](../citilab-course-12-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
