[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en). Check it out!
[[/fact]]

### Controlling lights with hand claps

![Boards](boards.svg) Some boards have a built-in microphone: micro:bit V2, micro:STEAMakers, M5Stack-Core2 and others. For the Citilab ED1 and others you need to connect a microphone.

#### Detecting clap count

- With the **Sensing→Microphone** library it is possible to detect the number of times one claps their hands.

- This detection allows us to activate any element of the home display, for example the lighting.

- For Citilab ED1 and other borads the microphone is connected to the analog input **A1**.

![Analog microphone at A1](cm12-microfon-connexio.jpg)

The block to use it's **`clap count`**. It's important not to have other sounds that might interfere with detection.

![Clapping and Lights example](cm12-s1.png)

#### Activate the lights

- To simulate different lighting levels we can use leds, neopixels or the screen.

- We can use the **`fill display with _`** block that we will find in the **Graphics and Displays→Turtle** library instead of **`draw rectangle ...`** from the **TFT** library.

![Turtle library](cm12-01-turtle_library.png)

![Comparison of fill screen and draw rectangle](cm12-02-fillscreen_rectangle.png)

##### Challenge 1: Build a program with different lighting levels controlled by clapping your hands.

![Challenge](cm-challenge-en.png)

<details><summary>Solution to challenge 1</summary>

![Solution challenge 1](cm12-s2.png)

</details>

![Plates](boards.svg) For boards with LED screen that allows color change (Citilab ED1, micro:STEAMakers, M5Stack-Core and others) we can define a new block, using the advanced block **`set display color _`**.

![Omple pantalla LED](cm12-02-omple-pantallaled.png)

[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en).

| [⬅️ Previous activity](../citilab-course-11-en) | [Next activity ➡️](../citilab-course-13-en) |
| ----------------------------------------------- | ------------------------------------------- |

[[/fact]]
