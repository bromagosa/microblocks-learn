[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Il·luminació controlada picant de mans

![Plaques](boards.svg) Algunes plaques porten micròfon incorporat: micro:bit V2, micro:STEAMakers, M5Stack-Core2 i d'altres. En el cas de la Citilab ED1 i d'altres, cal connectar un micròfon.

#### Detectant el recompte d'aplaudiments

 Amb la llibreria **Sensors→Micròfon** és possible detectar el nombre de vegades que piquem de mans.

- Aquesta detecció ens permet activar qualsevol element de casa nostra, per exemple la il·luminació.

- En el cas de la Citilab ED1 cal connectar un micròfon a l’entrada analògica **A1**.

![Micròfon analògic A1](cm12-microfon-connexio.jpg)

El bloc que retorna el nombre d'aplaudiments és **`recompte d'aplaudiments`**. Cal que no hi hagi altres sorolls que puguin interferir en la detecció.

![Exemple il·luminació amb palmades](cm12-s1.png)

#### Activar les llums

- Per simular diferents nivells d'il·luminació podem fer servir leds, neopíxels o la pantalla.

- Podem fer servir el bloc **`omple la pantalla de color _`** que trobarem a la llibreria **Gràfics i pantalles→Tortuga** en comptes del de **`dibuixar rectangle a x _ y _ amplada _ alçada _ color _`** de la llibreria **TFT**.

![Llibreria Tortuga](cm12-01-turtle_library.png)

![Comparació omple pantalla i dibuixa rectangle](cm12-02-fillscreen_rectangle.png)

##### Repte 1: construeix un programa amb diferents nivells d’il·luminació controlats picant de mans.

![Challenge 1](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

![Solució repte 1](cm12-s2.png)

</details>

![Plaques](boards.svg) Per a plaques amb pantalla LED que permetin canvi de color (Citilab ED1, micro:STEAMakers, M5Stack-Core i d'altres) podem definir el següent bloc, fem us del bloc avançat **`fixa el color de la pantalla a _`**.

![Omple pantalla LED](cm12-02-omple-pantallaled.png)

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-11-ca) | [Activitat següent ➡️](../citilab-course-13-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
