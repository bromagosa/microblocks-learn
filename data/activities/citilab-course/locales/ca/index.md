### Curs de robòtica i electrònica creativa

En aquest curs aprendrem a programar d'una forma gràfica i intuïtiva dispositius electrònics i robots.

Es realitzaran projectes barrejant el món real de Microblocks amb el món virtual de Snap!, així com la connexió amb el protocol HTTP de la web. També es programaran dispositius de manera remota a través de connexió Bluetooth.

### Activitats

* [Primers passos](../citilab-course-01-ca)
* [Sensors bàsics](../citilab-course-02-ca)
* [Nivell de bombolla](../citilab-course-03-ca)
* [Dial virtual](../citilab-course-04-ca)
* [Jugant amb LEDs](../citilab-course-05-ca)
* [Jugar amb Neopíxels](../citilab-course-06-ca)
* [Visualitza el volum del so](../citilab-course-07-ca)
* [Alarma d'aparcament](../citilab-course-08-ca)
* [Theremin](../citilab-course-09-ca)
* [Persiana controlada amb botons](../citilab-course-10-ca)
* [Persiana controlada amb llum](../citilab-course-11-ca)
* [Il·luminació controlada picant de mans](../citilab-course-12-ca)
* [Eixugaparabrises](../citilab-course-13-ca)
* [Robot Fantàstic](../citilab-course-14-ca)
* [Robot Fantàstic amb memòria](../citilab-course-15-ca)
* [Control remot del robot Fantàstic amb infraroig](../citilab-course-16-ca)
* [Control remot del robot Fantàstic amb Wifi](../citilab-course-17-ca)
* [Control remot del robot Fantàstic amb Snap!](../citilab-course-18-ca)
* [Control remot del robot Fantàstic amb AppInventor](../citilab-course-19-ca)
* [Connexió entre MicroBlocks i Snap!](../citilab-course-20-ca)
