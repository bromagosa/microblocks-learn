### Creative electronics and robotics course

This is a course that the Citilab offers in-site and has been adapted to the MicroBlocks learn site.

In this course, you will learn how to program electronic devices and robots in a visual and intuitive way.

You will be tackling projects that mix the real world, using MicroBlocks, with the virtual one, using Snap! and the web HTTP protocol. You will also be programming devices remotely by means of Bluetooth.

### Activities

* [First steps](../citilab-course-01-en)
* [Basic sensor: light level and temperature](../citilab-course-02-en)
* [Bubble level](../citilab-course-03-en)
* [Virtual Dial](../citilab-course-04-en)
* [Visual alarm, beacon...](../citilab-course-05-en)
* [Play with Neopixels](../citilab-course-06-en)
* [View the sound volume](../citilab-course-07-en)
* [Parking alarm](../citilab-course-08-en)
* [Theremin](../citilab-course-09-en)
* [Shutter controlled with buttons](../citilab-course-10-en)
* [Shutter controlled with light](../citilab-course-11-en)
* [Lighting controlled by snapping hands](../citilab-course-12-en)
* [Windshield Wiper](../citilab-course-13-en)
* [Fantastic Robot](../citilab-course-14-en)
* [Fantastic robot with memory](../citilab-course-15-en)
* [Fantastic robot with infrared remote control](../citilab-course-16-en)
* [Remote control of Fantastic robot with Wifi](../citilab-course-17-en)
* [Remote Control Fantastic Robot with Snap!](../citilab-course-18-en)
* [Remote control of Fantastic robot with AppInventor](../citilab-course-19-en)
* [Connection between MicroBlocks and Snap!](../citilab-course-20-en)
