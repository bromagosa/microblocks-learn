[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en). Check it out!
[[/fact]]

### Controlling the Robot with Snap!

#### Communication from a web environment: Snap!

- As we have already seen in the previous unit, it is possible to communicate with the ED1 board through web requests (REST) ​​thanks to the fact that we can start a web server on it.

- The web browser is not the only one that can make this type of request, they can also be made from other applications.

- In this unit we will see how to control the robot from [Snap!](https://snap.berkeley.edu/).

<img title="Snap!" src="cm18-01-snap!.png" alt="" data-align="center" width="586">

#### Sending commands from Snap!

- **Snap!** is a block programming environment very similar to Scratch but which allows you to create more complex projects, thanks to the fact that it includes more advanced instructions than this one.

- One of these instructions in the form of a block is the one that allows web requests to be made in the same way as if they were made through the browser bar.

![URL block](cm18-02-url_block.png)

- **NOTE: for some reason it does not work in the Chrome browser and its derivatives, so it is recommended to use Firefox**.

##### Challenge 1: Control the robot with url blocks.

![Challenge](cm-challenge-en.png)

<details>
  <summary>Solution to challenge 1</summary>
    <img src="cm18-s1.png" title="Solution challenge 1">
</details>

##### Challenge 2: Control the robot with the arrow keys on your keyboard.

![Challenge](cm-challenge-en.png)

<details>
  <summary>Solution to challenge 1</summary>
    <img src="cm18-s2.png" title="Solution challenge 2">
</details>

[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en).

| [⬅️ Previous activity](../citilab-course-17-en) | [Next activity ➡️](../citilab-course-19-en) |
|--|--|

[[/fact]]
