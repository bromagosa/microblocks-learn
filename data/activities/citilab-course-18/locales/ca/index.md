[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Control remot del robot Fantàstic amb Snap!

#### Comunicació des d'un entorn web: Snap!

- Com ja hem vist a la unitat anterior, és possible comunicar-se amb la placa ED1 mitjançant peticions web (REST) gràcies a que podem posar en marxa un servidor web en aquesta.

- El navegador web no és l’únic que pot realitzar aquest tipus de peticions, ja també podem es poden fer desde altres aplicacions.

- En aquesta unitat veurem com control·lar el robot desde [Snap!](https://snap.berkeley.edu/).

![Snap!](cm18-01-snap!.png)

#### Enviar ordres des d'Snap!

- **Snap!** és un entorn de programació per blocs molt semblant a Scratch però que permet arribar a realitzar projectes més complexos, gràcies a que inclou instruccions més avançades que aquest.

- Una d’aquestes instruccions en forma de bloc és la que permet fer peticions web de igual forma que si es fessin mitjançant la barra del navegador.

![Bloc d'url](cm18-02-url_block.png)

- **NOTA: per alguna raó al navegador Chrome i els seus derivats no funciona, per lo que es recomana fer servir Firefox**.

##### Repte 1: controleu el robot amb blocs d'url.

![Challenge 1](cm-challenge.png)

<details>
  <summary>Solució al repte 1</summary>
    

![Solució repte 1](cm18-s1.png)


</details>

##### Repte 2: controleu el robot amb les fletxes del teclat.

![Challenge 2](cm-challenge.png)

<details>
  <summary>Solució al repte 2</summary>
    

![Solució repte 1](cm18-s2.png)


</details>

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-17-ca) | [Activitat següent ➡️](../citilab-course-19-ca) |
|--|--|

[[/fact]]
