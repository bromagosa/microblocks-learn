[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Robot Fantàstic

#### Robot Fantàstic

- El robot Fantàstic del Citilab es va dissenyar per treballar la robòtica a educació primària.

- El kit consta d’una placa ED1, dos servomotors i un xassís desmuntable. Munteu el robot seguint les instruccions de la caixa!

![Kit de robot](cm14-01-robot_kit.png)

![Robot muntat](cm14-02-robot_assembled.png)

#### Moviment del robot

- Per fer servir el robot Fantàstic és necessari afegir la llibreria **Kits i plaques→Motors ED1**. 

- El bloc **`mou motor 1 en _ i motor 2 en _ _ passos`** permet moure els motors simultàniament.

- Combinant el sentit horari i anti-horari del gir dels motors s’aconsegueix que el robot avanci, retrocedeixi o giri.

- Amb 512 passos el robot es mourà la distancia del perímetre de la roda. En el cas del Fantàstic són uns 22 cm.

- Per **moure 1 cm** cal fer uns **23 passos** de motor(aproixamadament 512/23). 

- Per **girar** el robot **1 grau** cal fer uns **2 passos** de motor. Aquest nombre depèn de la separació entre rodes i del seu diàmetre.

![Moure robot cap endavant](cm14-03-robot_forward.png)

##### Repte 1: controleu el robot amb els botons de direcció de la placa.

![Challenge 1](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

![Solució repte 1](cm14-s1.png)

</details>

#### Llibreria ED1 Fantàstic

- Per treballar en mil·límetres i graus es pot fer servir la llibreria **Robots→ED1 Fantàstic**.

![Blocs de la llibreria ED1 Fantàstic](cm14-04-blocs-fantastic.png)

- Per exemple, podem col·locar un adaptador per retolador i fer que el robot Fantàstic dibuixi una clàssica espiral de LOGO.

![Espiral Logo](cm14-05-espiral.png)

![Espiral ED1](cm14-06-espiral-fantastic.jpg)

#### Més sobre el robot Fantàstic

- Munta el teu propi robot Fantàstic

| Component                                                                                                 | Quantitat |
| ----------------------------------------------------------------------------------------------------- |:---------:|
| [Placa Citilab ED1](https://shop.innovadidactic.com/es/microblocks/1625-placa-ed1-8436574310009.html) | 1         |
| [Xassís per imprimir en 3D](https://cloud.citilab.eu/s/KeGcyB6KgK99kJF)                               | 1         |
| Motor 28BYJ-48 5V                                                                                     | 1         |
| Separador Hexagonal M3 25mm MF                                                                        | 4         |
| Separador Hexagonal M3 25mm FF                                                                        | 2         |
| Cargol M3 10 mm                                                                                       | 6         |
| Rosca M3 2,5mm                                                                                        | 2         |
| Goma roda  67mmx60mmx3,5mm                                                                            | 2         |
| Roll-on pel punt de gir DIN18                                                                         | 1         |

[Muntatge del kit](https://market.citilab.eu/ca/muntatge-del-kit/)

- Adaptadors per retolador 

[Disseny 3D de l'adaptador Stabilo hexagonal](https://www.tinkercad.com/things/cHA4LP92P0n-adaptador-stabilo)

[Disseny 3D de l'adaptador pel retolador Edding 1200](https://www.tinkercad.com/things/1dlaH5BsYZn-adaptador-edding)

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-13-ca) | [Activitat següent ➡️](../citilab-course-15-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
