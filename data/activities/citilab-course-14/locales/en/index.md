[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en). Check it out!
[[/fact]]

### Fantastic Robot

#### Fantastic Robot

- Citilab's Fantàstic robot was designed to work on robotics in primary education.

- The kit consists of an ED1 board, two servomotors and a detachable chassis. Assemble the robot following the instructions in the box!

![KRobot kit](cm14-01-robot_kit.png)

![Assembled robot](cm14-02-robot_assembled.png)

#### Robot movement 

- The block **`move motor 1 in _ and motor 2 in _ _ steps`** allows to move the motors simultaneously.

- By combining the clockwise and counter-clockwise rotation of the motors, the robot can move forward, backward or turn.

- With 512 steps the robot moves around the perimeter of the wheel. In the case of the Fantastic they are about 22 cm.

- To **move 1 cm** you need to make about **23 engine steps** (approximately 512/22).

- To **rotate** the robot **1 degree** it is necessary to take about **2 steps**. This number depends on the distance between the wheels and their diameter.

![Mpve robot forward](cm14-03-robot_forward.png)

##### Challenge 1: Control the robot with the directional buttons on the board.

![Challenge](cm-challenge-en.png)

<details><summary>Solution to challenge 1</summary>

![Solution challenge 1](cm14-s1.png)

</details>

#### ED1 Fantàstic library
To work in millimeters and degrees, you can use the **Robots→ED1 Fantàstic library**.

![ED1 Fantàstic library blocks](cm14-04-blocs-ed1-fantastic.png)

- For example, we can place a marker adapter and make the Fantàstic robot draw a classic LOGO spiral.

![Espiral Logo](cm14-05-espiral.png)

![Espiral ED1](cm14-06-espiral-fantastic.jpg)

#### More about the Fantastic robot

- Assemble your own Fantàstic Robot

| Component                                                                                             | Quantitat |
| ----------------------------------------------------------------------------------------------------- |:---------:|
| [Citilab ED1 board](https://shop.innovadidactic.com/es/microblocks/1625-placa-ed1-8436574310009.html) | 1         |
| [Chassis to print on a 3D printer](https://cloud.citilab.eu/s/KeGcyB6KgK99kJF)                        | 1         |
| Motor 28BYJ-48 5V                                                                                     | 1         |
| Hexagonal Separator M3 25mm MF                                                                        | 4         |
| Hexagonal Separator M3 25mm FF                                                                        | 2         |
| Screw M3 10 mm                                                                                        | 6         |
| Thread M3 2,5mm                                                                                       | 2         |
| Rubber wheel 67mmx60mmx3,5mm                                                                          | 2         |
| Roll-on by the turning point DIN18                                                                    | 1         |

[Kit Assembly](https://market.citilab.eu/en/kit-assembly/)

- Marker adapters

[3D design for adapter for Stabilo hexagonal marker](https://www.tinkercad.com/things/cHA4LP92P0n-adaptador-stabilo)

[3D design for adapter for Edding 1200 marker](https://www.tinkercad.com/things/1dlaH5BsYZn-adaptador-edding)


[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en).

| [⬅️ Previous activity](../citilab-course-13-en) | [Next activity ➡️](../citilab-course-15-en) |
|--|--|

[[/fact]]
