[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en). Check it out!
[[/fact]]

### Theremin

#### Theremin

- The [Theremin](https://ca.wikipedia.org/wiki/Theremin) is an electronic musical instrument that is played with the hands but without contact.

- The tone of the instrument varies depending on the distance to the hand. Use the other hand to adjust the volume.

[![Sheldon's Theremin](https://img.youtube.com/vi/_YYABE0R3uA/0.jpg)](https://www.youtube.com/watch?v=_YYABE0R3uA)

#### Theremin without volume control

![Boards](boards.svg) Boards: Citilab ED1, micro:bit, micro:STEAMakers, M5Stack-Core, Boardie and others with integrated speaker.

- We will use the touch block **`play frequency _ for _ ms`**, adapting the distance values 0-500) to the frequency values 100-5000).

![Simple theremin with volume control](cm09-02-theremin1.png)

- We will notice that the sound is not continuous, as the sensor takes time to read. To solve it, you need to make two separate programs, one that does the reading and another that reproduces the sound.

##### Challenge 1: Make a Theremin, without volume control, with the distance sensor.

![Challege](cm-challenge-en.png)

<details><summary>Solution to challenge 1</summary>

![Solution challenge 1](cm09-s1.png)

</details>

#### Theremin with volume control

![Boards](boards.svg) Boards: Citilab ED1, micro:bit V2, micro:STEAMakers, M5Stack-Core and others with DAC or fast PWM.

- To adjust the volume of the generated tone we can use the **System→sound Prims** library.

- With the **`DAC write _`** block we can generate a wave of the desired frequency where the volume is affected by the values;

- We can connect a potentiometer to analog pin 1 to control the volume.

![Theremin with volume control](cm09-03-theremin2.png)

[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en).

| [⬅️ Previous activity](../citilab-course-08-en) | [Next activity ➡️](../citilab-course-10-en) |
|--|--|

[[/fact]]

