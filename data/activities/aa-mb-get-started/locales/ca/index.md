### Connectar la micro:bit

**Nou a MicroBlocks?** Consulteu [Primeres passes](https://microblocks.fun/get-started).

Obriu MicroBlocks i connecteu la micro:bit a l'ordinador amb un cable USB.

Si executeu MicroBlocks al navegador, feu clic a la icona USB per fer la connexió.
Seleccioneu el port on està connectada la micro:bit i seguiu les instruccions que es mostraran al navegador.
Apareixerà un cercle verd indicant que s'ha realitzat la connexió.

![](connectionCircle.png =450x*)

L'aplicació d'escriptori MicroBlocks es connectarà automàticament a la micro:bit.

### Botons

Des de *Control*, arrossegueu a l'àrea de programació dos blocs `quan es premi el botó`.

![](controlCategory.png =200x*)

Feu servir el menú per canviar A per B en un d'ells.

![](buttonAHat.png) ![](buttonBHat.png)


Seleccioneu la llibreria pantalla LED.

![](LEDDisplayLibrary.png =200x*)

Feu servir aquests scripts per fer que els botons de la micro:bit encenguin i apaguin la pantalla LED.

![](buttonAFace.png) ![](buttonBClear.png)

Editeu el bloc de la pantalla fent clic en els requadres petits. Fent clic en un requadre i arrossegant el ratolí podreu canviar múltiples quadrats.

### Animacions

Creeu animacions de lletres, números, formes i símbols amb els blocs `pantalla LED`, `espera` i `repeteix`. Podeu escriure el vostre nom?

![](animation-hey.png =240x*) ![](animation-face.png =240x*) ![](animation-shapes.png =240x*)

Feu servir els blocs barret `quan es premi el botó` per iniciar animacions. Com podeu controlar la velocitat de l'animació?

### Desplaçament de Text

Arrossegueu el bloc `anima el text` a la zona de programació i feu clic en el text per personalitzar-lo!

![](scrollText.png)

També podeu animar números. Proveu aquest script per crear un compte enrere!

![](scroll321Go.png)

