### 连接 micro:bit

**micro:bit 新手？** 请参考 [开始](https://microblocks.fun/get-started).

打开 MicroBlocks 并使用 USB 数据线将 micro:bit 连接到你的计算机。

如果在浏览器中运行 MicroBlocks，请单击 USB 图标进行连接。
MicroBlocks 桌面应用程序将自动连接到 micro:bit。

出现一个绿色圆圈，表明 micro:bit 已连接。

![](connectionCircle.png =150x*)

### 按钮

从 *控制* 中，将两个 **当按键被按下** 积木块拖入脚本区域。

![](controlCategory.png =140x*)

通过下拉菜单将其中一个积木块的 A 更改为 B。

![](buttonAHat.png) ![](buttonBHat.png)

选择 LED 显示屏库。

![](LEDDisplayLibrary.png =160x*)

通过以下脚本，你现在可以用 micro:bit 上的按钮打开和关闭 LED 显示屏：

![](buttonAFace.png) ![](buttonBClear.png)

单击小框来调整显示积木块。单击可改变一个框，也可用鼠标向下拖动来改变多个框。

### 动画

使用显示、等待和重复积木块来创建字母、数字、形状和符号的动画。你能说出你的名字吗？

![](animation-hey.png) ![](animation-face.png) ![](animation-shapes.png)

使用按钮来启动动画。如何控制动画速度呢？

### 滚动文本

将`滚动显示文字`积木块拖到脚本区域，然后单击文本进行自定义。

![](scrollText.png)

你还可以滚动显示数字。试试用这个脚本来创建倒计时！

![](scroll321Go.png)
