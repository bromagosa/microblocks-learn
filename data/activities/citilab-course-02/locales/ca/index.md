[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Sensors

![Plaques](boards.svg) Plaques: Citilab ED1, micro:bit, micro:STEAMakers, M5Stack-Core.

#### Sensors i operadors relacionals

- A més dels botons, moltes plaques incorporen diferents sensors: llum, temperatura, inclinació, etc.

![Sensor ED1](cm02-01-sensors.png)

- Donat que aquests sensors donen un valor analògic (és a dir, numèric) haurem de fer servir **blocs relacionals** si volem executar accions relacionades amb aquest.

![Operadors](cm02-02-operadors.png)

- Aquests blocs també retornen un valor cert o fals, per tant, es poden fer servir amb els blocs condicionals que hem vist anteriorment.

![Llindar llum](cm02-03-llindar.png)

##### Repte 1: mostra dos dibuixos diferents a la pantalla al tapar i destapar el sensor de llum

![Challenge](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

  ![Solució repte 1](cm02-s1.png)

</details>

#### Sensor de temperatura

- El sensor de temperatura és un component que s'incorpora habitualment a la placa.

- Un exemple molt senzill que es pot fer és afegir a la gràfica el valor que ens dona el sensor cada X temps (per exemple 1 segon) per visualitzar-ho còmodament.

![Gràfic temperatura](cm02-04-grafic-temperatura.png)

- Inclús podem exportar les dades en format .csv i obrir-les amb un programa de full de càlcul (MSOffice, LibreOffice, etc.)

![Exportar CSV](cm02-05-grafic-csv.png)

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-01-ca) | [Activitat següent ➡️](../citilab-course-03-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
