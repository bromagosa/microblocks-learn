[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en). Check it out!
[[/fact]]

### Sensors

![Boards](boards.svg) Boards: Citilab ED1, micro:bit, micro:STEAMakers, M5Stack-Core.

#### Sensors and relational operators

- In addition to the buttons, the ED1 board incorporates different sensors: light, temperature, tilt, etc.

![ED1 Sensors](cm02-01-sensors.png)

- Given that these sensors give an analog value (that is, numerical) we will have to use **relational blocks** if we want to execute actions related to it.

![Operators](cm02-02-operadors.png)

- These blocks also return a true or false value, so they can be used with the conditional blocks we saw earlier.

![Light Threshold](cm02-03-llindar.png)

##### Challenge 1: Show two different patterns on the screen by covering and uncovering the light sensor

![Challenge](cm-challenge-en.png)

<details><summary>Solution to challenge 1</summary>

  ![Solution challenge 1](cm02-s1.png)

</details>

#### Temperature sensor

- Another sensor integrated into the board is the temperature sensor.

- A very simple example that can be done is to add to the graph the value that the sensor gives us every X time (for example 1 second) to visualize it comfortably.

![Temperature graph](cm02-04-grafic-temperatura.png)

- We can even export the data in .csv format and open it with a spreadsheet program (MSOffice, LibreOffice, etc.)

![Export CSV](cm02-05-grafic-csv.png)

[[fact]]
This activity is part of the [Citilab Course](../citilab-course-en).

| [⬅️ Previous activity](../citilab-course-01-en) | [Next activity ➡️](../citilab-course-03-en) |
| ----------------------------------------------- | ------------------------------------------- |

[[/fact]]
