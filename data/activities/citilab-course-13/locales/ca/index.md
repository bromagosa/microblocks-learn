[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Eixugaparabrises

![Plaques](boards.svg) Citilab ED1. Per altres plaques és necessària una placa adaptadora.

#### Motors pas a pas

- Els motors pas a pas són un tipus de servomotors que giren un nombre determinat de passos en un sentit o el contrari.

- Destaquen per la seva precisió i versatilitat. Permeten controlar tant l'angle de rotació com el nombre de voltes amb gran exactitud. Són els motors que fan servir les impressores 3D per posicionar el capçal.

- Un model típic i econòmic és el **28BYJ-48**. Funciona a **5 Volts** i permet fer 512 passos per una volta.

![Motors pas a pas](cm13-01-stepper_motors.png)

![Informació steppers](cm13-02-stepper_motors_info.png)

#### Funcionament dels motors pas a pas

- Aquests motors tenen varis bobinats que es van activant alternativament per aconseguir el moviment rotatori.

![Motor pas a pas, funcionament](cm13-03-coils.png)

- Per cada bobinat hi ha un cable específic per activar-ho.

- Per fer-los servir es necessita un circuit específic “driver” que apliqui les tensions adequades.

- La placa Citilab ED1, incorpora 2 sortides preparades per aquest tipus de motors.

| ![Connexió steppers ED1](cm13-05-stepper_connection.png) | ![Informació steppers](cm13-04-circuit_scheme.png) |
| --------------------------------------------------------------- | -------------------------------------------------- |

- Per altres plaques cal un adaptador, que es connecta a 4 sortides digitals.

![Driver 28BYJ-48](cm13-08-driver.jpg)

#### Llibreria Motors ED1

- Per treballar amb els motors pas a pas 28BYJ-48 cal fer servir la llibreria **Motors ED1** que es troba a la categoria **Kits i plaques**.

![Llibreria motors pas a pas](cm13-06-stepper_library.png)

- Hi ha blocs per treballar amb els 2 motors independentment i un específic per moure els dos motors a la vegada.

![Blocs motors pas a pas](cm13-07-stepper_blocks.png)

- Només per usuaris avançats: El bloc **`set motor pin _ for motor _ to _`** serveix per controlar les sortides dels motors de la placa ED1 com a pins digitals extra .

##### Repte 1: fes un eixugaparabrises amb la velocitat controlada per un potenciòmetre.

![Challenge 1](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

![Solució repte 1](cm13-s1.png)

</details>

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [⬅️ Activitat anterior](../citilab-course-12-ca) | [Activitat següent ➡️](../citilab-course-14-ca) |
| ------------------------------------------------ | ----------------------------------------------- |

[[/fact]]
