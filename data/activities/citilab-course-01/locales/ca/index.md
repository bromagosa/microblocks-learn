[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!
[[/fact]]

### Introducció

#### Què és MicroBlocks

* Microblocks és un entorn de programació lliure que permet treballar amb diferents plaques electròniques (Citilab ED1, micro:bit, M5Stack-Core, micro:STEAMaker, etc.) i robots (3dBot, CoCube,  ED1 Fantàstic, etc).
* El codi es visualitza a l’ordinador però s’executa directament a la placa. Això fa que aquesta es pugui desconnectar i seguir funcionant igual sense cap operació addicional.
* Està disponible tant per instal·lar a [diferents sistemes](https://microblocks.fun/releases) (Windows, MacOS, Linux)  com per fer-ho servir de manera [online](https://microblocks.fun/run/microblocks.html).  També hi ha disponibles versions per descarregar i executar sense necessitat d'instal·lar (standalone executables).
* La versió web té com a avantatge que sempre està actualitzada. Les versions descarregables detecten les plaques automàticament i són més ràpides.

![El mateix bloc a diferents plaques](cm01-01-bloc-pantalla.png)

#### Característiques ED1 (Part frontal)

![ED1 frontal](cm01-02-ED1-frontal.png)

#### Característiques ED1 (Part darrere)

![ED1 darrere](cm01-03-ED1-darrere.png)

#### Instal·lació i connexió amb placa

* Amb algunes plaques i per alguns sistemes operatius, cal instal·lar prèviament un controlador (driver)  perquè el sistema reconegui la placa. En el cas de la ED1 i altres plaques com la  STEAMaker es necessita [aquest controlador](https://www.silabs.com/developer-tools/usb-to-uart-bridge-vcp-drivers?tab=downloads) (Silabs dirver)  .  
* A continuació cal descarregar el programa directament d’[aquí](https://microblocks.fun/download) o bé anar a la versió web de [MicroBlocks](https://microblocks.fun/run/microblocks.html) (es requereix una versió recent de Chrome o compatible).
* Si la placa no està preparada per MicroBlocks s'haurà d'instal·lar el firmware corresponent amb l'opció **actualitza firmware a la placa** al menú ![Gear Menu](icon-gear.svg) configuració.
* Si l'ordinador reconeix la placa, el programa s’hauria de connectar automàticament amb aquesta.  A la versió web cal fer la connexió manualment clicant a la icona **Connecta**. En aquest cas apareix un menú que ens demana si ens volem connectar per **USB** pel Bluetooth **BLE** o **obrir Boardie**. De moment escollir USB, la resta d'opcions les veurem més endavant.

| ![Connexió USB navegador](cm01-04-connectar1.png) | ![Indicador connexió USB](cm01-04-connectar2.png) |
| ------------------------------------------------- | ------------------------------------------------- |

#### Blocs i llibreries

Com a molts entorns de programació per blocs, disposem d’una paleta de blocs dividits en **categories**.

![Categories](cm01-05-categories.png)

També tenim disponibles una sèrie de **llibreries,** que són conjunts de blocs amb un únic propòsit. Alguns d’aquests estan formats per altres blocs.

![Llibreries](cm01-06-llibreries.png)

El programa per defecte carregarà unes llibreries o unes altres depenent de la placa connectada. Si tenim desactivada l'opció avançada **auto-carrega llibreries de la placa** o carreguem un programa que no les inclou, no apareixeran.

![Llibreries per defecte](cm01-07-defecte.png)

### Primers passos

#### Comencem a programar

![Plaques](boards.svg) Plaques: Citilab ED1, micro:bit, micro:STEAMakers, M5Stack-Core, CoCube i d'altres amb botons i pantalla LED.

* Començarem fent servir el bloc **`pantalla`** de la categoria **Pantalla LED** agafant-lo i arrossegant-lo a l’àrea de programació.
* També agafarem el bloc **`en començar`** de la categoria **Control** i el connectarem amb l’anterior.
* Clicarem al botó de començar ![Botó Començar](icon-start.svg) o sobre el bloc **`en començar`** per veure com funciona.
* Desendollarem la placa, la com apagant-la i encenent-la per comprovar que el programa efectivament s’ha guardat en ella.

![Comencem a programar](cm01-08-comencem.png)

##### Repte 1: prova de fer altres dibuixos amb el bloc pantalla

![Challenge](cm-challenge.png)

<details><summary>Solució al repte 1</summary>

![Solució repte 1](cm01-s1.png)

</details>

#### Iteracions (I)

* Si volem repetir una acció varies vegades, per exemple fent un canvi de dibuix per efectuar una animació, podem utilitzar els blocs d’iteració com el **`per sempre`** o el **`repeteix _ vegades`** (entre d'altres) que tenim a la categoria **Control**.

![Blocs per sempre i repetir](cm01-09-iteracions.png)

* També caldrà posar una petita espera entre els blocs per a que doni temps de visualitzar els canvis de la pantalla, ja que si no ho farà massa ràpid i no es podrà apreciar.

![Bloc espera](cm01-10-espera.png)

##### Repte 2: prova de fer una animació que es repeteixi contínuament amb els blocs anteriors

![Challenge](cm-challenge.png)

<details><summary>Solució al repte 2</summary>

![Solució repte 2](cm01-s2.png)

</details>

#### Iteracions (II)

* Hi ha altres blocs per fer repeticions que també són molt útils, com ara el **`repeteix fins que _`** i el **`per cada i en _`**.
* En el primer, la repetició es repeteix fins que es doni una condició, per exemple si premem un botó de la placa.

![Bloc repeteix fins](cm01-11-repeteix-fins.png)

* En el cas del segon, el valor de **i** començarà en **1** i anirà creixent a cada iteració fins arribar al valor establert (per defecte 10). També serveix per recórrer llistes, com veurem més endavant.

![Bloc per cada](cm01-12-per-cada.png)

##### Repte 3: prova de fer un compte enrere de 5 a 0 amb un bloc de repetició.

![Challenge accepted](cm-challenge.png)

<details><summary>Solució al repte 3</summary>

![Solució repte 3](cm01-s3.png)

**O també:**

![Solució repte 3b](cm01-s3b.png)

</details>

#### Botons i condicionals

* A la placa Citilab ED1 el **botó A** correspon al **botó OK** i el **botó B** al **botó X**. Per fer servir la resta de botons s'ha d'utilitzar la llibreria **Botons ED1**.  En el cas que no es carregui automàticament, podem afegir-la clicant al botó (**Afegir Llibreria**) i anant a **Kits i plaques→Botons ED1**.
  
  ![Llibreria botons ED1](cm01-13-llibreria-botons.png)

* Els botons retornen un valor **digital (cert o fals)** representat com un interruptor **verd** o **vermell**.

![True](cm01-14-boto-ok0.png) ![False](cm01-15-boto-ok1.png)

Aquest tipus de bloc es pot ficar en qualsevol altre que tingui representada la ranura amb la mateixa forma.

![Blocs condicionals](cm01-16-blocs-codicionals.png)

##### Repte 4: mostra un dibuix a la pantalla i neteja’l fent servir dos botons.

![Challenge](cm-challenge.png)

<details><summary>Solució al repte 4 (quans)</summary>

![Solució repte 4](cm01-s4a.png)

</details>

<details><summary>Solució al repte 4 (per sempre)</summary>

![Solució repte 4](cm01-s4b.png)

</details>

[[fact]]
Aquesta activitat forma part del [Curs del Citilab](../citilab-course-ca). Fes-li una ullada!

| [Activitat següent ➡️](../citilab-course-02-ca) |
| ----------------------------------------------- |

[[/fact]]
